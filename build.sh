#!/bin/sh

build_root="$(dirname $(realpath $0))/build"
cd "$build_root"
docker build -t static .
